Average calculator for LabVIEW
==============
can process 1-d or 2-d arrays. Look the pictures they say more.

### Front panel of user code
![](Documentation/test-front.jpeg)

### Block diagram of user code
![](Documentation/test-back.jpeg)  

### How it works
![](Documentation/Average-double.jpg)  

Open-source
============
Copyright (c) 2016, Ivan Kuvaldin. All rights reserved.
Licensed under [BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause).
